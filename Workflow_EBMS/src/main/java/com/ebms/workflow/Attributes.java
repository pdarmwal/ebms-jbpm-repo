package com.ebms.workflow;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class Attributes implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@org.kie.api.definition.type.Label(value = "correlationId")
	private java.lang.String correlationId;
	@org.kie.api.definition.type.Label(value = "containerId")
	private java.lang.String containerId;
	@org.kie.api.definition.type.Label(value = "processDefId")
	private java.lang.String processDefId;

	public Attributes() {
	}

	public java.lang.String getCorrelationId() {
		return this.correlationId;
	}

	public void setCorrelationId(java.lang.String correlationId) {
		this.correlationId = correlationId;
	}

	public java.lang.String getContainerId() {
		return this.containerId;
	}

	public void setContainerId(java.lang.String containerId) {
		this.containerId = containerId;
	}

	public java.lang.String getProcessDefId() {
		return this.processDefId;
	}

	public void setProcessDefId(java.lang.String processDefId) {
		this.processDefId = processDefId;
	}

	public Attributes(java.lang.String correlationId,
			java.lang.String containerId, java.lang.String processDefId) {
		this.correlationId = correlationId;
		this.containerId = containerId;
		this.processDefId = processDefId;
	}

}